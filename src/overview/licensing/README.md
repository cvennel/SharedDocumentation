# Licensing


## Copyrights

You retain all copyrights over your contributions to LibreFoodPantry.


## Licenses and Certificate of Origin

By contributing to LibreFoodPantry, you agree to license
your source code contributions under
[GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) and
your content contributions under
[CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/),
and you are signing-off on the
[Developer Certificate of Origin 1.1](https://developercertificate.org/).


## Copyright Notices

We manage our copyright information by including a copyright notice in
each file we create that indicates the license that governs the file,
and by keeping the full text of the licenses in the root of each project.

There are two copyright notices: one for source code and one for content.
Here is the copyright notice for source code:

    Copyright (C) YEAR The LibreFoodPantry Authors.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

Here is the copyright notice for content:

    Copyright (C) YEAR The LibreFoodPantry Authors. This work is licensed
    under the Creative Commons Attribution-ShareAlike 4.0 International License.
    To view a copy of this license, visit
    http://creativecommons.org/licenses/by-sa/4.0/.

The copyright notice for source code should be placed in comments at the top
of each source file.

The copyright notice for content should be placed at the bottom of each content
file and should be visible to reader of the document (not just the source).


## Authorship information

Detailed authorship information is maintained by the Git version control system
and is available for each project using the `git log` command. When a release
is made for a project, an AUTHORS file is generated containing all authors
and co-authors listed in the `git log`.

If you make a commit that multiple authors helped to create (e.g., squashing a commits, pair programming, mob programming, etc.) it is important to give all
co-authors credit for their work. Do this by adding a `Co-authored-by:`
line to the end of your commit message for each co-author.
See [Creating a commit message with multiple authors](https://help.github.com/en/articles/creating-a-commit-with-multiple-authors) for more details.


---
Copyright &copy; 2019 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
