# LibreFoodPantry

[LOGO HERE]

[_sidebar.md](_sidebar.md ':include')

---
Copyright &copy; 2019 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
