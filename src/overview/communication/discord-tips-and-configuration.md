# Discord tips and configuration

## Account configuration

This will serve as a guide for configuring your personal Discord account.

Most of these settings maximize user privacy.

- Note: this guide should be used BEFORE joining any servers as some settings can't be added retroactively to previously joined servers.

Some settings only pertain to the desktop application or Windows version of Discord, these are labeled in the header.

After creating an account sign in to Discord (either through the Web UI or desktop application)


To edit your preferences, click the `User Settings` button.

![User icon screenshot](discord-screenshots/user-settings.PNG)

### User settings

#### Privacy & Safety

- Turn off all settings in this section to maximize privacy and transparency

### App settings

#### Voice & Video

This is where you configure your audio settings for voice chat.

- Test your microphone by clicking the `Let's Check` button to make sure Discord is correctly picking up your voice
- Use the default settings unless you find that Discord isn't properly picking up audio

Tips to fix Discord not picking up microphone audio (try each setting one at a time in order):
1. Disable `Automatically determine input sensitivity.` (you have to manually set the input gain)
2. Disable `Automatic Gain Control`
3. Disable `Echo Cancellation`
4. Disable `Noise Suppression`

#### Overlay (Windows desktop app setting)

- Disable the `in-game overlay` (this should only impact any applications Discord detects as a video game)

#### Notifications

This is a personal preference setting.

- Note you can configure notifications on a per-server and per-channel level (shown later in this document), this is more recommended than turning off all notifications

If you do not want any notifications from Discord, disable the following settings:

1. `Enable Desktop Notifications`
2. `Enable Unread Message Badge`
3. `Enable Taskbar Flashing` (desktop application setting)

#### Keybinds (Desktop app setting)

You can create keybinds to automatically perform certain actions.

- A good one is to add a keybind for `Toggle Mute` for muting your microphone input in Discord

#### Game Activity (Desktop app setting)

Discord automatically will show any application you are currently running that it detects as a video game as a status message below your account name on servers.

- Disable `Display currently running game as a status message.` to get rid of this

#### Appearance

Most of these settings are based on personal preference, change them to your liking.

If you find that the Discord desktop application impacts your computer's performance:

- Disable `Hardware Acceleration` (this will make Discord animations appear slower)

#### Windows Settings (Windows desktop app setting)

These are all personal preferences.

- Disable `Open Discord` to prevent Discord from automatically running when starting your operating system.

## User server settings:

This will show the different personal settings you can configure for a server you have joined.

Right click the server icon to edit its settings.

- Clicking `Server Mute` disables all notifications for the server
- Clicking `Notification Settings` will let you edit the server's notification settings
- Clicking `Privacy Settings` disables the ability for server members to send messages directly to your Discord account
- Clicking `Change Nickname` lets you change how your name will appear to everyone on the server

### Notification Settings

There are many different ways to configure a server's notifications.

- Disable `Mobile Push Notifications` if you have the Discord application on your mobile device and do not want to see notifications on this.

The recommended settings would be to manually add the category for each of the projects that you are not involved in and set the notifications to only `MENTIONS`. This way you still receive messages specifically directed to you.

- Leave all notifications on for the `announcements` channel (under the Community category) as this is where important messages are posted

Example:
---
![Example server notification settings screenshot](discord-screenshots/server-notification-settings.PNG)
- In this case all project categories and their associated channels are muted (except for specific mentions), along with the general `text` channel under the community category

## Discord pop-ups:

Sometimes when joining a new server or after a certain period of time, Discord will display various pop-up messages in the top-left pane.


To get rid of these messages, click the `X` button in the top-right corner of the message.

Currently there is no user setting to prevent these messages from appearing.

### Example pop-up message
![Discord pop-up message example](discord-screenshots/popup-message.PNG)

---
Copyright &copy; 2019 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
