# About


## Vision

LibreFoodPantry is a vibrant, welcoming community of clients, users, and developers who believe in developing and maintaining humanitarian projects. We enhance computer science education through involvement in instructor-led, free and open source software projects that support local food pantries.


## Mission

Our mission is to expand a community of students and faculty across multiple institutions who believe software can be used to help society. We strive to support local food pantries with quality, adaptable, free and open source software (FOSS) to help them serve their guests. Through learning opportunities within FOSS food pantry projects, we provide students with the perspective that computing can be used for social good.


## Values

To ensure a healthy and safe environment in which to collaborate and learn, and to help establish and promote effective development practices, we have adopted the following values. We expect all community members to read and uphold these values.

* [Code of conduct](../code-of-conduct/)
* [Agile values](https://agilemanifesto.org/)
* [Agile principles](https://agilemanifesto.org/principles.html)
* [FOSSisms](https://opensource.com/education/14/6/16-foss-principles-for-educators)


## Governance

LibreFoodPantry is guided by a Coordinating Committee comprised of
Trustees and Shop Managers. This body typically meets weekly.

- [CC Meeting Board](https://gitlab.com/groups/LibreFoodPantry/-/boards/1468199)
- [Meeting agendas and minutes prior to 2020-02-07](https://drive.google.com/open?id=12cbsPfniz38ZsOOz0NcaChl4W_Apn_-s)


### Trustees

- Darci Burdge, Nassau Community College, since 2019
- Heidi Ellis, Western New England University, since 2019
- Greg Hislop, Drexel University, since 2019
- Stoney Jackson, Western New England University, since 2019
- Lori Postner, Nassau Community College, since 2019
- Karl Wurst, Worcester State University, since 2019


### Shop Managers

Shop managers manage a shop of developers that contribute to one or more
LFP projects. Shop managers and shop developers receive elevated privileges
on the projects their shops contribute to.

- Lori Postner, Nassau Community College, Fall 2019
- Robert Walz, Western New England University, Fall 2019
- Heidi Ellis, Western New England University, Fall 2019
- Darci Burdge, Nassau Community College, Spring 2019, Spring 2020
- Karl Wurst, Worcester State University, Spring 2019, Spring 2020
- Stoney Jackson, Western New England University, Spring 2019, Spring 2020


---
Copyright &copy; 2019 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
