# What's New

## 2020-02-28

### Added

- [A landing page for education](https://librefoodpantry.org/#/education/).

### Fixed

- Link to PDF of paper on education landing page.

## 2019-12-28

### Added

- An overview of shops.

### Refactor

- Significant reorganization of contents to make it easier to find information
  and to add new documentation.


## 2019-12-27

### Added
- CHANGELOG (src/CHANGELOG.md)
- No-Fork Workflow (src/documentation/no-fork-workflow)


---
Copyright &copy; 2019 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
