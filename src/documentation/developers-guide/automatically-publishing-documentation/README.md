# Automatically Publishing Documentation

A project can be configured to automatically publish its documentation every
time a merge request is successfully merged into its `master` branch.
To configure your project for automatic publication of its documentation

1. Register your project with librefoodpantry.gitlab.io
2. Configure your project to trigger librefoodpantry.gitlab.io


## Register your project with librefoodpantry.gitlab.io

LibreFoodPantry/librefoodpantry.gitlab.io is responsible for gathering the
documentation from all registered projects.

To register a project with LibreFoodPantry/librefoodpantry.gitlab.io
you must add an entry for the project in librefoodpantry.gitlab.io's
`projects.json` file.

**projects.json**
```json
{
  "librefoodpantry.gitlab.io": {
    "repo": "https://gitlab.com/LibreFoodPantry/librefoodpantry.gitlab.io.git",
    "copy": [
      {
        "source": "src",
        "dest": ""
      },
      {
        "source": "docs",
        "dest": "projects/librefoodpantry.gitlab.io"
      }
    ]
  },
  "SharedDocumentation": {
    "repo": "https://gitlab.com/LibreFoodPantry/SharedDocumentation.git",
    "copy": [
      {
        "source": "src",
        "dest": ""
      }
    ]
  }
}
```

This file contains two entries, one for `liberfoodpantry.gitlab.io` and one for
`SharedDocumentation`.

Most projects should use the following as a template to register itself with
librefoodpantry.gitlab.io.

**Common Template Entry**
```json
  "<project-name>": {
    "repo": "<https-url-to-projects-repository>",
    "copy": [
      { "source": "docs", "dest": "projects/<project-name>" }
    ]
  }
```

## Configure your project to trigger librefoodpantry.gitlab.io

To trigger librefoodpantry.gitlab.io to pull your documentation everytime a
merge request is merged into `master`, you need to add a .gitlab-ci.yml file
with the contents as shown below.  Or you must adjust your .gitlab-ci.yml to
add the `trigger-cross-projects` stage and the corresponding `website` job.


.gitlab-ci.yml from SharedDocumentation
```yaml
# From: https://about.gitlab.com/handbook/marketing/product-marketing/demo/#cross-project-pipeline-triggering-and-visualization-may-2019---1110
# And: https://docs.gitlab.com/ee/ci/multi_project_pipelines.html#creating-multi-project-pipelines-from-gitlab-ciyml

stages:
- test
- trigger-cross-projects


test:
  stage: test
  script:
    - echo "fake test"
  only:
  - branches


website:
    stage: trigger-cross-projects
    variables:
        ENVIRONMENT: 'Deploy documentation to website'
    trigger: LibreFoodPantry/librefoodpantry.gitlab.io
    only:
    - master
```
