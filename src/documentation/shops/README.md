# Shops

A shop is a group of developers charged with working on a subset of LFP.

## Shop Managers

A shop manager is a member of the of the shop and a member of the LFP Coordinating Committee. The shop manager is responsible for the activities of their shop and grants access and privileges as necessary to their team members.

## Shop Members

Members of a shop answer to the shop manager, and carry out development activities within the shop's charge.

## A Shop's Charge

A shop is given authority and responsibility over some part (modules, features, and or components) of LFP. Typically shops' charges do not overlap. If an when they do, the overlapping shops' managers must regularly coordinate to ensure smooth development and operations. Charges may change over time, and is managed by the Coordinating Committee.

A shop may, and probably should, further subdivide and delegate its authority over modules, features, and components to teams within the shop.


---
Copyright &copy; 2019 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
